import styled from "styled-components/native";

export const ContainerLogo = styled.View`
  align-items: center;
  background-color: rgba(176, 147, 238, 0.6);
`;

export const Logo = styled.Image`
  width: 222;
  height: 33;
  margin: 20px 0px;
`;
