import React from "react";
import { ContainerLogo, Logo } from "./style";

function Header() {
  return (
    <ContainerLogo>
      <Logo source={require("../../../assets/ImagemLogo.png")}></Logo>
    </ContainerLogo>
  );
}

export default Header;
