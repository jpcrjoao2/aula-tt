import React, { useState } from "react";
import { View, TouchableOpacity } from "react-native";
import {
  ContainerProduct,
  ImgContainer,
  ProductImg,
  InfoContainer,
  TextInfoContainer,
  InfoText,
  InfoText1,
  ButtonInfoContainer,
  ButtonText,
  Button,
} from "./style";

import { Ionicons } from "@expo/vector-icons";

import { FontAwesome } from "@expo/vector-icons";

type ProductInformation = {
  name: string;
  price: number;
  img: string;
  type: string;
  isMainPage: Boolean;
};

function Product({ name, img, type, price, isMainPage }: ProductInformation) {
  let imgBg: string;
  const [isFavorite, setIsFavorite] = useState(false);
  let starColor: string;

  switch (type) {
    case "Hp":
      imgBg = "#717c79";
      break;

    case "Marvel":
      imgBg = "#d56e69";
      break;

    case "Disney":
      imgBg = "#ab4eab";
      break;

    default:
      imgBg = "#fbfbfb";
  }

  if (isFavorite) {
    starColor = "#ffce31";
  } else {
    starColor = "#5e5e5e";
  }

  return (
    <ContainerProduct>
      <ImgContainer style={{ backgroundColor: `${imgBg}` }}>
        <ProductImg source={require(`../../../assets/${img}.png`)}></ProductImg>
      </ImgContainer>
      <InfoContainer>
        <TextInfoContainer>
          <View>
            <InfoText>{name} Funko</InfoText>
          </View>

          <View>
            <InfoText1> Preço: {price}</InfoText1>
          </View>
        </TextInfoContainer>

        {isMainPage ? (
          <ButtonInfoContainer>
            <TouchableOpacity>
              <Button>
                <ButtonText>Comprar</ButtonText>
              </Button>
            </TouchableOpacity>
          </ButtonInfoContainer>
        ) : (
          <ButtonInfoContainer>
            <TouchableOpacity onPress={() => setIsFavorite(!isFavorite)}>
              <FontAwesome
                name="star"
                size={24}
                style={{ color: `${starColor}` }}
              />
            </TouchableOpacity>

            <TouchableOpacity>
              <Ionicons name="ios-trash-outline" size={35} color="black" />
            </TouchableOpacity>
          </ButtonInfoContainer>
        )}
      </InfoContainer>
    </ContainerProduct>
  );
}

export default Product;
