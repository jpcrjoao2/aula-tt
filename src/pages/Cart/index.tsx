import React from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

import { FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";

import { FontAwesome } from "@expo/vector-icons";

import {
  Container,
  TextCarrinho,
  TextContainer,
  ContainerLogo,
  Logo,
  ButtonInfoContainer,
  ContainerProduct,
  ImgContainer,
  InfoContainer,
  InfoText,
  InfoText1,
  ProductImg,
  TextInfoContainer,
} from "./style";
import { product } from "../../constants/product";
import Product from "../../components/Product";
import Header from "../../components/Header";

function Carrinho() {
  return (
    <Container>
      <Header />
      <TextContainer>
        <TextCarrinho>Carrinho</TextCarrinho>
      </TextContainer>

      {product.map((produto) => (
        <Product
          key={produto.id}
          name={produto.name}
          price={produto.price}
          img={produto.img}
          type={produto.type}
          isMainPage={false}
        />
      ))}
    </Container>
  );
}
//exportacao
export default Carrinho;
