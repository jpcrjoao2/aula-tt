import styled from "styled-components/native";
//@ts-ignore
import background from "../../../assets/ImgCart/Rectangle100.png";

export const Container = styled.View`
  flex: 1;
  background-image: url(${background});
`;

export const TextContainer = styled.View`
  justify-content: center;
  align-items: center;
`;

export const TextCarrinho = styled.Text`
  font-size: 24px;
  font-weight: 800;
  margin-top: 25;
  font-family: Calibri;
`;

export const ContainerLogo = styled.View`
  align-items: center;
  background-color: rgba(176, 147, 238, 0.6);
`;

export const Logo = styled.Image`
  width: 222;
  height: 33;
  margin: 20px 0px;
`;
export const ContainerProduct = styled.View`
  width: 370;
  height: 150;
  align-self: center;
  align-content: center;
  justify-content: center;
  flex-direction: row;
  margin-top: 32;
`;

export const ImgContainer = styled.View`
  width: 150;
  height: 150;
  background-color: #fbfbfb;
  align-items: center;
  justify-content: center;
  border-radius: 20;
`;

export const ProductImg = styled.Image`
  width: 100px;
  height: 125px;
  border-radius: 5;
`;

export const InfoContainer = styled.View`
  background-color: #d8f5e5;
  width: 212px;
  height: 150px;
  margin-left: 10;
  border-radius: 5;
`;

export const TextInfoContainer = styled.View`
  margin-left: 10;
  margin-top: 18;
  display: inline;
  justify-content: space-around;
`;

export const InfoText = styled.Text`
  font-size: 15px;
  font-weight: 800;
`;

export const InfoText1 = styled.Text`
  font-size: 15px;
  font-weight: 800;
  margin-top: 18;
`;

export const ButtonInfoContainer = styled.View`
  display: flex;
  justify-content: flex-end;
  gap: 112px;
  flex-direction: row;
  align-items: center;
  margin-top: 30;
  margin-right: 15;
`;

export const Button = styled.View`
  background-color: #857cd4;
  border-radius: 5px;
  padding: 5px 10px;
`;

export const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: 700;
`;
