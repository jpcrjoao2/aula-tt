import React from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

import { FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";

import {
  Container,
  TextCarrinho,
  TextContainer,
  ContainerLogo,
  Logo,
  ButtonInfoContainer,
  ContainerProduct,
  ImgContainer,
  InfoContainer,
  InfoText,
  InfoText1,
  ProductImg,
  TextInfoContainer,
  Button,
  ButtonText,
} from "./style";
import Header from "../../components/Header";
import Product from "../../components/Product";
import { product } from "../../constants/product";

function Carrinho() {
  return (
    <Container>
      <Header />
      <TextContainer>
        <TextCarrinho>Home</TextCarrinho>
      </TextContainer>

      {/* mapear arry */}
      {product.map((produto) => (
        <Product
          key={produto.id}
          name={produto.name}
          price={produto.price}
          img={produto.img}
          type={produto.type}
          isMainPage={true}
        />
      ))}
    </Container>
  );
}
//exportacao
export default Carrinho;
