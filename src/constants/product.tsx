export const product = [
  {
    id: 1,
    name: "Harry Potter",
    price: 120,
    img: "HarryPotter",
    imgBg: "#717c79",
    type: "Hp",
  },
  {
    id: 2,
    name: "Homem de Ferro",
    price: 200,
    img: "IronMan",
    imgBg: "#d56e69",
    type: "Marvel",
  },
  {
    id: 3,
    name: "Rapunzel",
    price: 150,
    img: "Rapunzel",
    imgBg: "#ab4eab",
    type: "Disney",
  },
];
